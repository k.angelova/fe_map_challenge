import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DataService } from 'src/app/services/data/data.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.subscription = this.dataService.getHotelsData().subscribe();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
