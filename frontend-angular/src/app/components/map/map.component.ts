import {
  Component,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnDestroy,
} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import H from '@here/maps-api-for-javascript';
import onResize from 'simple-element-resize-detector';
import { environment } from 'src/environments/environment';
import { DataService } from 'src/app/services/data/data.service';
import { Hotel } from 'src/app/models/Hotel.model';
import { SubSink } from 'subsink';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements AfterViewInit, OnDestroy {
  private map?: H.Map;
  @ViewChild('map') mapDiv?: ElementRef;

  /* Defult marker icon */
  private markerIcon = new H.map.Icon('assets/icons/home-icon.svg', {
    size: { w: 30, h: 30 },
  });

  /* Active marker icon */
  private activeMarkerIcon = new H.map.Icon(
    'assets/icons/home-icon-active.svg',
    {
      size: { w: 50, h: 50 },
    }
  );

  /* Map group that stores markers */
  private markersGroup = new H.map.Group();

  /*default - Munich center*/
  private defaultCenter = { lat: 48.137154, lng: 11.576124 };

  subSink = new SubSink();

  constructor(private dataService: DataService) {}

  ngAfterViewInit(): void {
    if (!this.map && this.mapDiv) {
      // instantiate a platform, default layers and a map as usual
      const platform = new H.service.Platform({
        apikey: environment.apiKey,
      });
      const layers = platform.createDefaultLayers();
      const map = new H.Map(
        this.mapDiv.nativeElement,
        layers.vector.normal.map,
        {
          pixelRatio: window.devicePixelRatio,
          center: this.defaultCenter,
          zoom: 16,
        }
      );
      var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

      // Create the default UI components
      var ui = H.ui.UI.createDefault(map, layers);

      onResize(this.mapDiv.nativeElement, () => {
        map.getViewPort().resize();
      });
      this.map = map;

      this.subSink.sink = this.dataService.hotels$.subscribe(
        (data: Hotel[]) => {
          if (data.length > 0) {
            this.addHotelMarkers(data);
          }
        }
      );

      this.subSink.sink = this.dataService.selectedHotel$.subscribe(
        (hotel: Hotel | null) => {
          this.updateMarkersAndCenterMap(hotel);
        }
      );
    }
  }

  addHotelMarkers(data: Hotel[]): void {
    let self = this;
    data.forEach((hotel: Hotel, index) => {
      let marker = new H.map.Marker(hotel.position);
      marker.setIcon(this.markerIcon);
      marker.setData(hotel);
      this.markersGroup.addObject(marker);
    });

    this.markersGroup.addEventListener('tap', function (evt: any) {
      self.dataService.setSelectedHotel(evt.target.getData());
    });

    this.map!.addObject(this.markersGroup);
  }

  updateMarkersAndCenterMap(hotel: Hotel | null) {
    this.markersGroup.getObjects(false).map((marker: any) => {
      if (marker.getData().id === hotel?.id) {
        marker.setIcon(this.activeMarkerIcon);
        marker.setZIndex(1);
      } else {
        marker.setIcon(this.markerIcon);
        marker.setZIndex(undefined);
      }
    });
    if (hotel) {
      this.map?.setCenter(hotel.position, true);
    } else {
      this.map?.setCenter(this.defaultCenter, true);
    }
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
    this.markersGroup.dispose();
  }
}
