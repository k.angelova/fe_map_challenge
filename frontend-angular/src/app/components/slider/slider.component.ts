import { ViewportScroller } from '@angular/common';
import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostListener,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { Hotel } from 'src/app/models/Hotel.model';
import { DataService } from 'src/app/services/data/data.service';
import { ModalService } from 'src/app/services/modal/modal.service';
import { SwiperOptions } from 'swiper';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SliderComponent implements OnInit, OnDestroy {
  @ViewChild('swiper') swiper: any;

  captionMsg = 'km from the city center';
  additionalMsg = 'Designs may vary';
  selectedHotelSubscription = new Subscription();
  selectedHotel: Hotel | null;
  swiperSlidesPerView = 1;
  defaultCardWidth = 370;

  constructor(
    public dataService: DataService,
    private modalService: ModalService,
    private cdr: ChangeDetectorRef
  ) {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.swiperSlidesPerView = window.innerWidth / this.defaultCardWidth;
    this.selectedHotelSubscription = this.dataService.selectedHotel$.subscribe(
      (hotel: Hotel | null) => {
        this.selectedHotel = this.dataService.getSelectedHotel();
        if (this.selectedHotel) {
          this.swiper.swiperRef.slideTo(this.selectedHotel.id);
        }
      }
    );
    this.cdr.detectChanges();
  }

  @HostListener('window:resize', ['$event'])
  onWindowResize() {
    this.swiperSlidesPerView = window.innerWidth / this.defaultCardWidth;
  }

  onSlideChange() {
    if (
      this.selectedHotel &&
      this.selectedHotel.id !== this.swiper.swiperRef.activeIndex
    ) {
      const hotel =
        this.dataService.getHotels()[this.swiper.swiperRef.activeIndex];
      this.dataService.setSelectedHotel(hotel);
    }
  }

  openBookingForm(hotel: Hotel) {
    this.modalService.open();
    this.dataService.setSelectedHotel(hotel);
  }
  ngOnDestroy(): void {
    this.selectedHotelSubscription.unsubscribe();
  }
}
