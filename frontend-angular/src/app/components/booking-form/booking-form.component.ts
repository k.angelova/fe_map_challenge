import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { ModalService } from 'src/app/services/modal/modal.service';
import { DataService } from 'src/app/services/data/data.service';

@Component({
  selector: 'app-booking-form',
  templateUrl: './booking-form.component.html',
  styleUrls: ['./booking-form.component.scss'],
})
export class BookingFormComponent {
  bookingForm: FormGroup;
  hotelTitle: string;
  display$: Observable<'open' | 'close'>;
  onSubmitErrorMsg: string;

  constructor(
    private fb: FormBuilder,
    private modalService: ModalService,
    public dataService: DataService
  ) {}

  ngOnInit() {
    this.initForm();
    this.display$ = this.modalService.watch();
  }

  private initForm() {
    this.bookingForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: [
        '',
        [
          Validators.required,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
        ],
      ],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
    });
  }

  onSubmit() {
    if (
      new Date(this.bookingForm.get('startDate')?.value) <
      new Date(this.bookingForm.get('endDate')?.value)
    ) {
      this.modalService.close();
      setTimeout(() => {
        alert('Successful reservation');
      }, 200);
    } else {
      this.onSubmitErrorMsg = 'Start date must be before end date!';
    }
  }

  close() {
    this.onSubmitErrorMsg = null;
    this.modalService.close();
  }
}
