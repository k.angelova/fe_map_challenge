import { Address } from './Address.model';
import { Position } from './Position.model';

export class Hotel {
  id: number;
  title: string;
  address: Address;
  position: Position;
  distance: number;
  price: number;
}
