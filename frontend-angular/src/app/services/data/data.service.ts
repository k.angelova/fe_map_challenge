import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable, of, Subject, Subscriber } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError, tap } from 'rxjs/operators';
import { query } from '@angular/animations';
import { Hotel } from 'src/app/models/Hotel.model';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  private baseEndpoint = environment.apiUrl;

  private _hotels: BehaviorSubject<Hotel[]> = new BehaviorSubject<Hotel[]>([]);
  public readonly hotels$: Observable<Hotel[]> = this._hotels.asObservable();

  private _selectedHotel = new BehaviorSubject<Hotel | null>(null);
  public readonly selectedHotel$: Observable<Hotel | null> =
    this._selectedHotel.asObservable();

  markerSelectedEvent: Subject<string> = new Subject<string>();
  // default location, intersection of equator and prime meridian
  private _currentLocation: [latitude: number, longitude: number] = [0, 0];

  get currentLocation(): [latitude: number, longitude: number] {
    return this._currentLocation;
  }
  set currentLocation(position: [latitude: number, longitude: number]) {
    this._currentLocation = position;
  }

  constructor(private _httpClient: HttpClient) {
    this._hotels.subscribe();
  }

  getHotelsData() {
    return this._httpClient.get(`${this.baseEndpoint}/hotels`).pipe(
      tap(({ items }: any) => {
        let hotels = items.map((item: any, index: number) => {
          item.id = index;
          item.price = Math.round(Math.random() * 100 + 1);
          return item;
        });
        this._hotels.next(hotels);
        this.setSelectedHotel(hotels[0]);
      }),
      catchError(() => {
        this._hotels.next([]);
        this.setSelectedHotel(null);
        return of([]);
      })
    );
  }

  getHotels() {
    return this._hotels.getValue();
  }

  setSelectedHotel(hotel: Hotel | null) {
    this._selectedHotel.next(hotel);
  }

  getSelectedHotel() {
    return this._selectedHotel.getValue();
  }

  queryLocation(query: string) {
    const [latitude, longitude] = this._currentLocation;
    const params = new HttpParams();
    params.append('at', `${latitude},${longitude}`);
    params.append('q', query);
    params.append('apiKey', environment.apiKey);
    return this._httpClient
      .get(
        `https://discover.search.hereapi.com/v1/discover?apiKey=${environment.apiKey}&at=${latitude},${longitude}&q=${query}`
      )
      .pipe(
        tap(({ items }: any) => {
          let hotels = items.map((item: any) => {
            item.price = Math.round(Math.random() * 100 + 1);
            return item;
          });
          this._hotels.next(hotels);
        }),
        catchError(() => {
          this._hotels.next([]);
          return of([]);
        })
      );
  }
}
